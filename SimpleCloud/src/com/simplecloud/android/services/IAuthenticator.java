package com.simplecloud.android.services;

public interface IAuthenticator {
    public void login();
    public void logout();
    public boolean isLogin();
}

