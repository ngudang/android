package com.simplecloud.android.services;

import android.app.ProgressDialog;

import com.simplecloud.android.background.CopyFilesAsync.OnListViewProgressBarUpdateListener;
import com.simplecloud.android.models.files.DirectoryObject;
import com.simplecloud.android.models.files.FileObject;
import com.simplecloud.android.models.files.FileSystemObject;

/** All files related actions
 * @author ngu
 *
 */
public interface IFileManager {

    /** Update the directory and its children from database. Requires running on background
     * @param directory
     */
    public void loadDirectoryFromDatabase(DirectoryObject directory);
    
    /**
     * Update the directory and its children from the cloud service
     * @param directory
     */
    public void loadDirectoryFromCloud(DirectoryObject directory);
    
    public void copyFiles(OnListViewProgressBarUpdateListener mAdapterListener);
    
    public int deleteFile(FileObject file);

    public int deleteEmptyDirectory(DirectoryObject directory);

    public int createEmptyFile(DirectoryObject parentDir, String name);

    public int createEmptyFolder(DirectoryObject parentDir, String name);

    public int renameFile(FileObject f, String newName);

    public int renameDirectory(DirectoryObject d, String newName);
    
    public String shareLink(FileSystemObject f);
    
    /**
     * Download the file into a preconfigured local directory. Requires running on background
     * @param f  the file to be downloaded
     * @param dialog  progress dialog of the process
     * @return  the error code 
     */
    public int downloadFileToCache(FileObject f, ProgressDialog dialog);
    
}
